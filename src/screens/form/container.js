import { connect } from "react-redux";
import FromComponent from "./component";
import {
  registerAction,
  registeredSuccessfully,
  saveCountries
} from "../../store/actions";

const mapStateToProps = state => ({
  data: state.data,
  countries: state.countries
});

const mapDispatchToProps = dispatch => ({
  register: data => dispatch(registerAction(data)),
  registeredSuccessfully: () => dispatch(registeredSuccessfully()),
  saveCountries: data => dispatch(saveCountries(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FromComponent);
