import React, { Component } from "react";
import { checkValidations } from "../../utils/validations";
import {
  Button,
  FormControl,
  Dropdown,
  DropdownButton,
  ButtonToolbar,
  InputGroup,
  Form
} from "react-bootstrap";
import { Group, Label, Control, Text } from "react-bootstrap/Form";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import allInputFields from "./inputFields";

export default class FromComponent extends Component {
  componentDidMount() {
    const { saveCountries, countries } = this.props;
    if (countries.length === 0) {
      fetch("https://restcountries.eu/rest/v2/all")
        .then(response => response.json())
        .then(result => {
          const allCountries = result.map(country => ({
            name: country.name,
            numericCode: country.numericCode
          }));
          saveCountries(allCountries);
        });
    }
  }

  /**
   * This function will handle all the
   * inputs given by user and set into
   * the particular state
   */
  handleInputChange = (event, countryCode) => {
    const { register, data } = this.props;
    let value = event.target.value;
    const key = event.target.name;
    if (key === "country") {
      value = countryCode;
    }
    const updatedData = {
      ...data,
      [key]: value
    };
    register(updatedData);
  };

  /**
   * This method will submit the form
   * and clear all the text fields
   */
  handleSubmit = event => {
    event.preventDefault();
    const { registeredSuccessfully } = this.props;
    registeredSuccessfully();
    toast.success("Registered Successfully");
  };

  /**
   * This function will disable the submit button
   * on specified conditions
   */
  isButtonDisable = data => {
    const { securityNumber, phoneNumber, emailAddress, country } = data;
    if (!securityNumber || !phoneNumber || !emailAddress || !country) {
      return true;
    }
    return false;
  };

  /**
   * This function will return the country name
   */
  getCountryName = () => {
    const { data, countries } = this.props;
    const { country } = data;
    return countries.find(countryObj => countryObj.numericCode === country);
  };

  render() {
    const { data, countries } = this.props;
    const errorMessages = checkValidations(data);
    const keys = Object.keys(allInputFields);
    const selectedCountry = this.getCountryName();
    let isDisableButton =
      this.isButtonDisable(data) || Object.keys(errorMessages).length > 0;
    return (
      <Form className="formStyle" autoComplete="off">
        {keys &&
          keys.length > 0 &&
          keys.map((key, index) => {
            const field = allInputFields[key];
            return (
              <div key={index}>
                <Group controlId="formBasicEmail">
                  <Label>
                    <b>{field.text}</b>
                  </Label>
                  <Control
                    type="text"
                    name={field.value}
                    value={data[field.value] || ""}
                    onChange={this.handleInputChange}
                    placeholder={`Enter ${field.text}`}
                  />
                  <Text className="error">{errorMessages[field.value]}</Text>
                </Group>
              </div>
            );
          })}
        <Group controlId="formBasicEmail">
          <Label>
            <b>Country</b>
          </Label>
          <InputGroup>
            <DropdownButton
              as={InputGroup.Prepend}
              variant="outline-secondary"
              title="Countries"
              id="input-group-dropdown-1"
              name="country"
            >
              <Dropdown.Item
                value=""
                name="country"
                onClick={event => this.handleInputChange(event, "")}
              >
                Choose any country
              </Dropdown.Item>
              <Dropdown.Divider />
              {countries &&
                countries.length > 0 &&
                countries.map((country, index) => (
                  <Dropdown.Item
                    key={index}
                    value={country.numericCode}
                    name="country"
                    selected={country.numericCode === country}
                    onClick={event =>
                      this.handleInputChange(event, country.numericCode)
                    }
                  >
                    {country.name}
                  </Dropdown.Item>
                ))}
            </DropdownButton>
            <FormControl
              disabled
              value={selectedCountry ? selectedCountry.name : ""}
              aria-describedby="basic-addon1"
            />
          </InputGroup>
          <Text className="error">{errorMessages["country"]}</Text>
        </Group>

        <ButtonToolbar>
          <Button
            variant="primary"
            size="lg"
            disabled={isDisableButton}
            onClick={this.handleSubmit}
          >
            Submit
          </Button>
        </ButtonToolbar>
        <ToastContainer autoClose={3000} />
      </Form>
    );
  }
}
