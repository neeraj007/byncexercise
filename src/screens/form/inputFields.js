export default {
  securityNumber: {
    text: "Sweden Security Number",
    value: "securityNumber"
  },
  phoneNumber: {
    text: "Phone Number",
    value: "phoneNumber"
  },
  emailAddress: {
    text: "Email Address",
    value: "emailAddress"
  }
};
