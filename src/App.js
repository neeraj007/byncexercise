import React from "react";
import logo from "./assets/images/logo.png";
import mobileImage from "./assets/images/mobileImage.png";
import FormContainer from "./screens/form/container";
import { Row, Col } from "react-bootstrap";
import "./App.css";

const App = () => (
  <div className="App">
    <Row>
      <Col className="col1">
        <img src={logo} alt="Internet issue" />
        <p className="about">
          With us you can manage your loan directly via the Bynk app.
        </p>
        <img src={mobileImage} alt="Internet issue" height="500px" />
      </Col>
      <Col className="col1">
        <h3 className="textColor">REGISTRATION FORM</h3>
        <FormContainer />
      </Col>
    </Row>
  </div>
);

export default App;
