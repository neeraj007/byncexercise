import { REGISTER, REGISTERED } from "../actions/register.action";
const initialState = {};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER: {
      const data = action.payload;
      return {
        ...state,
        ...data
      };
    }
    case REGISTERED: {
      return {};
    }

    default:
      return state;
  }
};

export default registerReducer;
