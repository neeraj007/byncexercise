import registerReducer from "./register.reducer";
import countriesReducer from "./country.reducer";
import { combineReducers } from "redux";

export default combineReducers({
  data: registerReducer,
  countries: countriesReducer
});
