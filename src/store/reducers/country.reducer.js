import { SAVE_COUNTRIES } from "../actions";
const initialState = [];

const countriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_COUNTRIES: {
      const data = action.payload;
      return data;
    }

    default:
      return state;
  }
};

export default countriesReducer;
