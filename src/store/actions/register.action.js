export const REGISTER = "REGISTER";
export const REGISTERED = "REGISTERED";

export const registerAction = data => ({
  type: REGISTER,
  payload: data
});

export const registeredSuccessfully = data => ({
  type: REGISTERED,
  payload: {}
});
