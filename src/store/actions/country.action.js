export const SAVE_COUNTRIES = "SAVE_COUNTRIES";

export const saveCountries = data => ({
  type: SAVE_COUNTRIES,
  payload: data
});
