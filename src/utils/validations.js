/**
 * This function will check
 * email is valid or not
 */
function isValidateEmail(value) {
  const regex = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  if (!value) {
    return "This Field is required";
  }
  if (!regex.test(value)) {
    return "Invalid Email address";
  }
  return "";
}

/**
 * This function will check
 * phone number is in correct
 * format or not
 */
function isValidPhoneNumner(value) {
  if (!value) {
    return "This Field is required";
  }
  const number = value
    .replace("+46", "")
    .replace("0046", "")
    .replace(/ /g, "");
  const length = number.length;
  const regex = new RegExp(
    /^(([+]|[0]{2})46)?\s*?([0]?[0-9]{1,3})\s*?([0-9]{3,4})\s*?([0-9]{3,4})$/
  );
  if (!regex.test(value) || (length < 8 || length > 10)) {
    return "Invalid phone number";
  }
  return "";
}

/**
 * This function will return
 * the checksum of any security number
 */
function getChecksum(allDigits) {
  const length = allDigits.length;
  let sum = 0;
  for (let i = 0; i < length - 1; i += 1) {
    let digit = Number(allDigits[i]);
    if (!isNaN(digit)) {
      if (i % 2 === 0) {
        digit *= 2;
        if (digit > 9) {
          digit = digit - 9;
        }
      }
      sum += digit;
    }
  }

  return sum;
}

/**
 * This function will check that
 * sweden security number is
 * valid or not
 */
function isSecurityNumberValid(value) {
  if (!value) {
    return "This Field is required";
  }
  const allDigits = value.split("").filter(value => !isNaN(Number(value)));
  const length = allDigits.length;
  if (length === 10) {
    const sum = getChecksum(allDigits);
    if (sum % 10 !== Number(allDigits[length - 1])) {
      return "Invalid last digit";
    }
  }
  const regex = new RegExp(/^(\d{6}([-+])\d{4}|\d{10})$/);
  if (!regex.test(value)) {
    return "Invalid security number";
  }
  return "";
}

function checkValidations(data) {
  const { securityNumber, phoneNumber, emailAddress, country } = data;
  const errorMessages = {};

  if (emailAddress !== undefined) {
    const emailErrorMessage = isValidateEmail(emailAddress);
    if (emailErrorMessage !== "") {
      errorMessages["emailAddress"] = emailErrorMessage;
    }
  }
  if (phoneNumber !== undefined) {
    const phoneErrorMessage = isValidPhoneNumner(phoneNumber);
    if (phoneErrorMessage !== "") {
      errorMessages["phoneNumber"] = phoneErrorMessage;
    }
  }
  if (securityNumber !== undefined) {
    const securityErrorMessage = isSecurityNumberValid(securityNumber);
    if (securityErrorMessage !== "") {
      errorMessages["securityNumber"] = securityErrorMessage;
    }
  }
  if (country !== undefined && country === "") {
    errorMessages["country"] = "Country is required";
  }
  return errorMessages;
}

export {
  isValidateEmail,
  isValidPhoneNumner,
  isSecurityNumberValid,
  checkValidations
};
